//
//  RecipeDetailsViewController.swift
//  RecipeTutorial
//
//  Created by Lutful Kabir on 13/10/19.
//  Copyright © 2019 Lutful Kabir. All rights reserved.
//

import UIKit

class RecipeDetailsViewController: UIViewController {

    var movieTitle:String? = nil
    var movieCast:String? = nil
    var recipeType:String? = nil
    var moviePlot:String? = nil
    
    @IBOutlet var poster: UIImageView!
    @IBOutlet var cast: UITextView!
    @IBOutlet var details: UITextView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = movieTitle
        self.poster.image = UIImage(named: movieTitle!)
        self.cast.text = self.movieCast;
        self.details.text = self.moviePlot;
    }
    
}
