//
//  RecipeDetailsViewController.swift
//  RecipeTutorial
//
//  Created by Lutful Kabir on 13/10/19.
//  Copyright © 2019 Lutful Kabir. All rights reserved.
//

import UIKit

//Movie Array************
let MovieData = [
    ["title": "Ghostbusters", "cast": "Kate McKinnon, Leslie Jones, Melissa McCarthy, Kristen Wiig", "plot":"After the members of a team of scientists (Harold Ramis, Dan Aykroyd, Bill Murray) lose their cushy positions at a university in New York City, they decide to become 'ghostbusters' to wage a high-tech battle with the supernatural for money. They stumble upon a gateway to another dimension, a doorway that will release evil upon the city. The Ghostbusters must now save New York from complete destruction", "genre": "easy"],
  ["title": "Central Intelligence", "cast": "Dwayne Johnson, Kevin Hart", "genre": "easy"],
  ["title": "Bad Moms", "cast": "Mila Kunis, Kristen Bell, Kathryn Hahn, Christina Applegate", "genre": "easy"],
  ["title": "Jason Bourne", "cast": "Matt Damon, Alicia Vikander, Julia Stiles", "genre": "hard"],
  ["title": "Suicide Squad", "cast": "Margot Robbie, Jared Leto, Will Smith", "genre": "hard"],
  ["title": "Star Trek Beyond", "cast": "Chris Pine, Zachary Quinto, Zoe Saldana", "genre": "hard"],
  ["title": "Deadpool", "cast": "Ryan Reynolds, Morena Baccarin, Gina Carano", "genre": "hard"],
  ["title": "London Has Fallen", "cast": "Gerard Butler, Aaron Eckhart, Morgan Freeman, Angela Bassett", "genre": "hard"],
  ["title": "Keanu", "cast": "Jordan Peele, Keegan-Michael Key", "genre": "easy"],
  ["title": "Neighbors 2: Sorority Rising", "cast": "Seth Rogen, Rose Byrne", "genre": "easy"],
  ["title": "The Shallows", "cast": "Blake Lively", "genre": "normal"],
  ["title": "The Finest Hours", "cast": "Chris Pine, Casey Affleck, Holliday Grainger", "genre": "normal"],
  ["title": "10 Cloverfield Lane", "cast": "Mary Elizabeth Winstead, John Goodman, John Gallagher Jr.", "genre": "normal"],
  ["title": "A Hologram for the King", "cast": "Tom Hanks, Sarita Choudhury", "genre": "normal"],
  ["title": "Miracles from Heaven", "cast": "Jennifer Garner, Kylie Rogers, Martin Henderson", "genre": "normal"],
]
//****************

class RecipeViewController: UIViewController {

  @IBOutlet weak var tableView: UITableView!

  // The magic enum to end our pain and suffering!
  // For the most part the order of our cases do not matter.
  // What is important is that our first case is set to 0, and that our last case is always `total`.
  enum TableSection: Int {
    case hard, easy, normal, total
  }

  // This is the size of our header sections that we will use later on.
  let SectionHeaderHeight: CGFloat = 25

  // Data variable to track our sorted data.
  var data = [TableSection: [[String: String]]]()

  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationController?.navigationBar.topItem?.title = "Movie Name"
    let logoutBarButtonItem = UIBarButtonItem(title: "Recomandation", style: .done, target: self, action: #selector(logoutUser))
    self.navigationItem.rightBarButtonItem  = logoutBarButtonItem

    tableView.register(UINib(nibName: "RecipeCell", bundle: nil), forCellReuseIdentifier: "Cell")
    sortData()
  }
    
    @objc func logoutUser(){
         print("clicked")
    }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    tableView.reloadData()
  }

  // When generating sorted table data we can easily use our TableSection to make look up simple and easy to read.
  func sortData() {
    data[.hard] = MovieData.filter({ $0["genre"] == "hard" })
    data[.easy] = MovieData.filter({ $0["genre"] == "easy" })
    data[.normal] = MovieData.filter({ $0["genre"] == "normal" })
  }

}

extension RecipeViewController: UITableViewDataSource, UITableViewDelegate {
    
  func numberOfSections(in tableView: UITableView) -> Int {
    return TableSection.total.rawValue
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if let tableSection = TableSection(rawValue: section), let movieData = data[tableSection] {
      return movieData.count
    }
    return 0
  }

  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if let tableSection = TableSection(rawValue: section), let movieData = data[tableSection], movieData.count > 0 {
      return SectionHeaderHeight
    }
    return 0
  }

  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: SectionHeaderHeight))
    view.backgroundColor = UIColor(red: 253.0/255.0, green: 240.0/255.0, blue: 196.0/255.0, alpha: 1)
    let label = UILabel(frame: CGRect(x: 15, y: 0, width: tableView.bounds.width - 30, height: SectionHeaderHeight))
    label.font = UIFont.boldSystemFont(ofSize: 15)
    label.textColor = UIColor.black
    if let tableSection = TableSection(rawValue: section) {
      switch tableSection {
      case .hard:
        label.text = "Hard"
      case .easy:
        label.text = "Easy"
      case .normal:
        label.text = "Normal"
      default:
        label.text = ""
      }
    }
    view.addSubview(label)
    return view
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
    // Similar to above, first check if there is a valid section of table.
    // Then we check that for the section there is a row.
    if let tableSection = TableSection(rawValue: indexPath.section), let movie = data[tableSection]?[indexPath.row] {
      if let titleLabel = cell.viewWithTag(10) as? UILabel {
        titleLabel.text = movie["title"]
      }
      if let subtitleLabel = cell.viewWithTag(20) as? UILabel {
        subtitleLabel.text = movie["cast"]
      }
    }
    return cell
  }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailsPage: RecipeDetailsViewController = RecipeDetailsViewController()
        if let tableSection = TableSection(rawValue: indexPath.section), let movie = data[tableSection]?[indexPath.row]{
            detailsPage.movieTitle = movie["title"]
            detailsPage.movieCast = movie["cast"]
            detailsPage.recipeType = movie["genre"]
            detailsPage.moviePlot = movie["plot"]
            self.navigationController?.pushViewController(detailsPage, animated: true)
        }
        
        
//        self.navigationController?.pushViewController(recipeDetailsViewController, animated: true)
    }

}
